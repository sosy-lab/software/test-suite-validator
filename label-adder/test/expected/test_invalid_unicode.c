// This file is part of TestCov,
// a robust test executor with reliable coverage measurement:
// https://gitlab.com/sosy-lab/software/test-suite-validator/
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

#include<stdio.h>

int main() {
Goal_1:;

  int n = __VERIFIER_nondet_int();
  printf("%c\n", n);
  Goal_2:;
  
}
