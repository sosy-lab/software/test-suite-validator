// This file is part of TestCov,
// a robust test executor with reliable coverage measurement:
// https://gitlab.com/sosy-lab/software/test-suite-validator/
//
// SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void __VERIFIER_assume(int cond) {}

void __VERIFIER_error() {}

char __VERIFIER_nondet_char() { return 'a'; }

unsigned char __VERIFIER_nondet_uchar() { return 'a'; }

short __VERIFIER_nondet_short() { return 0; }

unsigned short __VERIFIER_nondet_ushort() { return 0; }

int __VERIFIER_nondet_int() { return 0; }

unsigned int __VERIFIER_nondet_uint() { return 0; }

long __VERIFIER_nondet_long() { return 0; }

unsigned long __VERIFIER_nondet_ulong() { return 0; }

long long __VERIFIER_nondet_longlong() { return 0; }

unsigned long long __VERIFIER_nondet_ulonglong() { return 0; }

float __VERIFIER_nondet_float() { return 0; }

double __VERIFIER_nondet_double() { return 0; }

_Bool __VERIFIER_nondet_bool() { return (_Bool)__VERIFIER_nondet_int(); }

void *__VERIFIER_nondet_pointer() { return (void *)__VERIFIER_nondet_ulong(); }

unsigned int __VERIFIER_nondet_size_t() { return __VERIFIER_nondet_uint(); }

unsigned char __VERIFIER_nondet_u8() { return __VERIFIER_nondet_uchar(); }

unsigned short __VERIFIER_nondet_u16() { return __VERIFIER_nondet_ushort(); }

unsigned int __VERIFIER_nondet_u32() { return __VERIFIER_nondet_uint(); }

unsigned int __VERIFIER_nondet_U32() { return __VERIFIER_nondet_u32(); }

unsigned char __VERIFIER_nondet_unsigned_char() {
  return __VERIFIER_nondet_uchar();
}

unsigned int __VERIFIER_nondet_unsigned() { return __VERIFIER_nondet_uint(); }

const char *__VERIFIER_nondet_string() { return 0; }